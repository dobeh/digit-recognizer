# ~/ml/kaggle/vEnv/bin python3

"""
Digit classifier script - Decision Tree Classifie
r
DATA FORMAT
Training set:
    28 x 28 image - total of 784
    1 extra column in training set (785) for the label (0 - 9)
    total in traing set 42000
Testing set:
    28 x 28 image - total of 784
    No labels
    total in testing set
"""

import cv2
import numpy as np
import collections
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.cross_validation import train_test_split


def draw_pixel(event, x, y, flags, params):

    # Get refs from global
    global draw, mouse_x, mouse_y

    mouse_x = x
    mouse_y = y

    if event == cv2.EVENT_LBUTTONDOWN:
        draw = True
    elif event == cv2.EVENT_LBUTTONUP:
        draw = False

    return


# Converts and Plots a 28 by 28 image.
def display_image(index, data_set):
    """
    Images are saved as inverse values for training purposes.
    Taking the value away from 255 and setting to grey scale give
    the expected visual output
    """
    image = data_set[index]
    image.shape = (28, 28)
    # plt.imshow(255 - image, cmap='gray')
    plt.imshow(image)
    print(image)
    plt.show()
    return


def invert_image(image):
    inverted_image = (255 - image)
    return inverted_image


def gini(labels):
    """ Calculate the Gini Impurity for a given list """
    impurity = collections.Counter(labels)
    total = len(labels)
    for label in impurity:
        probability_of_correct = impurity[label] / total
        impurity[label] = 1 - probability_of_correct
    return impurity


def output_submission_to_csv(data, filename):
    """ Outputs data to csv format for submission to kaggle """
    submission = pd.DataFrame()
    submission['Label'] = data
    submission.index += 1
    submission.index.name = 'ImageId'
    output_name = './' + filename + '.csv'
    submission.to_csv(output_name)
    return


# Import data
print("Importing data")
raw_training_data = pd.read_csv("./inputs/train.csv")
raw_test_data = pd.read_csv("./inputs/test.csv")

# Seperate headings, data and labels
training_headings = raw_training_data.loc[0, :]
training_data = raw_training_data.as_matrix()[:, 1:]
training_labels = raw_training_data.as_matrix()[:, 0]

# Check shape of training data
print("Training dataset shape:", training_data.shape)

# Impurity of training data set
print("Gini impurity:", gini(training_labels))

# Decision Tree Classifier #

# Training the decision tree - 70/30 split for training and testing
print("Training decision tree")
training_split = 10 / 10  # 100% used to train final tree for submission
end_training_index = int(len(training_data) * training_split)
start_testing_index = end_training_index + 1
dt_classifier = DecisionTreeClassifier()
dt_classifier.fit(training_data[:end_training_index],
                  training_labels[:end_training_index])

# Testing predictions from sample test data
# prediction = dt_classifier.predict(raw_test_data.as_matrix())
# output_submission_to_csv(prediction, "decision_tree_classifier_submission")


# Random Forest Classifier #

# Training the random forest classifier - 70/30 split
print("Training random forest")
x = training_data
y = training_labels
# Feature train and test, Label train and test
f_train, f_test, l_train, l_test = train_test_split(x, y, test_size=0.3)

rf_classifier = RandomForestClassifier()
rf_classifier.fit(training_data, training_labels)

# Testing predictions from sample test data
# prediction = rf_Classifier.predict(raw_test_data.as_matrix())
# output_submission_to_csv(prediction, "random_forest_classifier_submission")


# On the fly input mode for predictions

print("Training complete. Read for test inputs")

# load image to draw onto
img = np.zeros((28, 28), np.uint8)
img = cv2.rectangle(img, (0, 0), (28, 28), (255, 255, 255), -1)
cv2.namedWindow('image')
cv2.setMouseCallback('image', draw_pixel)

# variables used for test drawing mode
testing_mode = True
draw = False
mouse_x = 0
mouse_y = 0

while(testing_mode):
    cv2.imshow('image', img)
    key = cv2.waitKey(1) & 0xFF

    # backspace zeros image and redraws white
    if key == ord('\b'):
        img = np.zeros((28, 28), np.uint8)
        img = cv2.rectangle(img, (0, 0), (28, 28), (255, 255, 255), -1)

    # break loop and quit
    if key == ord('q'):
        testing_mode = False
        break

    # return key to feed into predictor
    if key == ord('\r'):
        # flattern image into a single vector
        test_img = img.flatten()
        # invert image as tree and forest are trained on inverse images
        test_img_inv = invert_image(test_img)
        # check predictions
        print("Random Forest Prediction",
              rf_classifier.predict([test_img_inv]))
        print("Decision Tree Prediction",
              dt_classifier.predict([test_img_inv]))

    # draws to image
    if draw is True:
        # Testing different brush types and how it effects accuracy
        # cv2.circle(img, (mouse_x, mouse_y), 1, (0, 0, 0), -1)
        cv2.rectangle(img, (mouse_x - 1, mouse_y - 1),
                      (mouse_x + 1, mouse_y + 1), -1)

print("Closing")
cv2.destroyAllWindows()
