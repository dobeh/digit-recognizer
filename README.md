
1. Requirements:   
    - python3
    - numpy
    - matplotlib
    - pandas
    - sklearn
    - scipy
    - opencv-python

    ``` python
    pip3 install numpy matplotlib pandas sklearn scipy opencv-python
    ```

1. Data Source: 
    - https://www.kaggle.com/c/digit-recognizer/data


1. Summary: 
    - run classifier.py to train decision tree and random forest
    - draw number in 28 x 28 grid
    - hit enter to test
    - backspace to img clear
    - q to exit

1. Tools:
    - Sublime text editor
    - PEP8 Linter